package main

import (
	"fmt"
	"log"

	"github.com/Shopify/sarama"
)

func main() {

	config := sarama.NewConfig()
	brokerAddress := []string{"localhost:9092"}

	master, err := sarama.NewConsumer(brokerAddress, config)
	if err != nil {
		panic(err)
	}
	defer func() {
		if err := master.Close(); err != nil {
			panic(err)
		}
	}()

	messageCountStart := 0
	var topicName string
	log.Printf("[*] Ввести: <Название темы>. Для выхода нажать CTRL+C")
	fmt.Scanf("%s", &topicName)

	consumer, err := master.ConsumePartition(topicName, 0, sarama.OffsetOldest)
	if err != nil {
		panic(err)
	}
	forever := make(chan bool)

	go func() {
		for {
			msg := <-consumer.Messages()
			messageCountStart++
			fmt.Printf("topic(%s)/partition(%d)/offset(%d): \"%s\" \n",
				msg.Topic, msg.Partition, msg.Offset, msg.Value)
		}
	}()
	<-forever
}
