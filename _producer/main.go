package main

import (
	"fmt"
	"log"

	"github.com/Shopify/sarama"
)

func main() {

	var (
		topicName string
		msgText   string
	)
	brockerAddress := []string{"localhost:9092"}

	config := sarama.NewConfig()
	config.Producer.Return.Successes = true
	producer, err := sarama.NewSyncProducer(brockerAddress, config)

	if err != nil {
		panic(err)
	}
	defer func() {
		if err := producer.Close(); err != nil {
			panic(err)
		}
	}()

	forever := make(chan bool)
	log.Println("[*] Вводить: <Название темы> <Текс Сообщения>. Для выхода нажать CTRL+C.")
	go func() {
		for {
			fmt.Scan(&topicName, &msgText)
			msg := &sarama.ProducerMessage{
				Topic: topicName,
				Value: sarama.StringEncoder(msgText),
			}
			partition, offset, err := producer.SendMessage(msg)
			if err != nil {
				panic(err)
			}
			fmt.Printf("Вы написали сообщение \"%s\" в => topic(%s)/partition(%d)/offset(%d)\n", msgText, topicName, partition, offset)
		}
	}()
	<-forever
}
